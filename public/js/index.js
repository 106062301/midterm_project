function init() {
    var all = false;
    var user_email = '';
    var client_email = '';
    var clientEmail = document.getElementById('clientEmail');
    var chats = document.getElementById('chatbox');
    var chat_history = firebase.database().ref('chat_history');
    var post_txt = document.getElementById('comment');
    var post_btn = document.getElementById('post_btn');

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='btnlogout'>Logout</span>";
            btnlogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function (result) {
                    alert("logout success");
                    window.location = 'signin.html';
                }).catch(function (error) {
                    alert("logout failed");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    clientEmail.addEventListener('keydown', function (event) {
        if (emailIsValid(clientEmail.value)) {
            if (event.key == 'Enter') {
                client_email = clientEmail.value;
                clientEmail.value = "";
                all = false;
                visitData();
            }
        } else if ((clientEmail.value == "All") || (clientEmail.value == "all") || (clientEmail.value == "ALL")) {
            if (event.key == 'Enter') {
                client_email = clientEmail.value;
                clientEmail.value = "";
                all = true;
                visitData();
            }
        }
    });
    function emailIsValid(email) {
        return /\S+@\S+\.\S+/.test(email)
    }

    function visitData() {
        chat_history.once('value', function (snapshot) {
            chats.innerHTML = '';
            var data = snapshot.val();
            for (var i in data) {
                if ((data[i].receiver == "ALL") && (all == true)) {
                    if (data[i].sender == user_email) {
                        var newPost = '<div>' +
                            '<span style="font-size:11px; float:right;">' + data[i].sender + ' ：' + '</span>' + '<br>' +
                            '<p class="userChat">' + data[i].content + '<br>' +
                            '<span style="font-size:10px; float:right;">' + data[i].time + '</span>' + '</p>' +
                            '</div>';
                        chats.innerHTML = chats.innerHTML + newPost;
                    } else if (data[i].sender != user_email) {
                        var newPost = '<div>' +
                            '<span style="font-size:11px; float:left;">' + data[i].sender + ' ：' + '</span>' + '<br>' +
                            '<p class="clientChat">' + data[i].content + '<br>' +
                            '<span style="font-size:10px; float:left;">' + data[i].time + '</span>' + '</p>' +
                            '</div>';
                        chats.innerHTML = chats.innerHTML + newPost;
                    }
                } else if (((data[i].sender == user_email) && (data[i].receiver == client_email)) || ((data[i].sender == client_email) && (data[i].receiver == user_email))) {
                    if (data[i].sender == user_email) {
                        var newPost = '<div>' +
                            '<span style="font-size:11px; float:right;">' + data[i].sender + ' ：' + '</span>' + '<br>' +
                            '<p class="userChat">' + data[i].content + '<br>' +
                            '<span style="font-size:10px; float:right;">' + data[i].time + '</span>' + '</p>' +
                            '</div>';
                        chats.innerHTML = chats.innerHTML + newPost;
                    } else {
                        var newPost = '<div>' +
                            '<span style="font-size:11px; float:left;">' + data[i].sender + ' ：' + '</span>' + '<br>' +
                            '<p class="clientChat">' + data[i].content + '<br>' +
                            '<span style="font-size:10px; float:left;">' + data[i].time + '</span>' + '</p>' +
                            '</div>';
                        chats.innerHTML = chats.innerHTML + newPost;
                    }
                } else continue;
            }
            chats.scrollTop = chats.scrollHeight;
        });
    }

    post_btn.addEventListener('click', function () {
        var date = new Date();
        var h = date.getHours();
        var m = date.getMinutes();
        if (h < 10)
            h = '0' + h;
        if (m < 10)
            m = '0' + m;
        var now = h + ':' + m;
        if (all == true) {
            client_email = "ALL";
            var postData = {
                sender: user_email,
                receiver: client_email,
                content: post_txt.value,
                time: now
            };
            chat_history.push(postData);
            post_txt.value = '';
        }
        else if (client_email != "") {
            var postData = {
                sender: user_email,
                receiver: client_email,
                content: post_txt.value,
                time: now
            };
            chat_history.push(postData);
            post_txt.value = '';
        }
    });

    chat_history.on('child_added', snapshot => {
        var data = snapshot.val();
        if (client_email == "") chats.innerHTML = '';
        else {
            var content = data.content;
            if (data.sender == user_email) {
                var newPost = '<div>' +
                    '<span style="font-size:11px; float:right;">' + data.sender + ' ：' + '</span>' + '<br>' +
                    '<p class="userChat">' + content + '<br>' +
                    '<span style="font-size:10px; float:right;">' + data.time + '</span>' + '</p>' +
                    '</div>';
                chats.innerHTML = chats.innerHTML + newPost;
            }
            else {
                var newPost = '<div>' +
                    '<span style="font-size:11px; float:left;">' + data.sender + ' ：' + '</span>' + '<br>' +
                    '<p class="clientChat">' + data.content + '<br>' +
                    '<span style="font-size:10px; float:left;">' + data.time + '</span>' + '</p>' +
                    '</div>';
                chats.innerHTML = chats.innerHTML + newPost;
                Pop();
            }
        }
        chats.scrollTop = chats.scrollHeight;
    });

    if (window.Notification) {
        var popNotice = function () {
            if (Notification.permission == "granted") {
                var notification = new Notification("Hi! " + user_email, {
                    body: client_email + " wants to contact you",
                    icon: 'img/notification_icon.png'
                });

                notification.onclick = function () {
                    notification.close();
                };
            }
        };

        function Pop() {
            if (Notification.permission == "granted") {
                popNotice();
            } else if (Notification.permission != "denied") {
                Notification.requestPermission(function (permission) {
                    popNotice();
                });
            }
        };
    } else {
        alert("Browser doesn't support Notification");
    }
}

window.onload = function () {
    init();
}