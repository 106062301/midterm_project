# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chat room
* Key functions (add/delete)
    1. chat with a person (type the email at the input email area)
    2. load message history
    3. chat with new user
    4. chat with all member (type "all" or "All" or "ALL" at the input email area)
    
* Other functions (add/delete)
    1. could see the time when the sender sent the message

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midtermproject-6db78.firebaseapp.com/

# Components Description : 
1. Membership Mechanism : As you register an email, the database would save it
                          and you could chat with all other members.
2. Third-Party Sign In : You could use Google to sign in
3. Chrome Notification : You will get a notification if someone sends you a message while you're online
4. Use CSS Animation : The sign in page fades in

# Other Functions Description(1~10%) : 
1. Time : You can find out the moment when the message is sent.

## Security Report (Optional)
1. The most basic security is the password system
2. The messages between two emails are accessible when one of the user signed in 
   and inputted the other email.
3. There is a global chanel to chat with all other members,
   but the messages are only accessible when a user signed in
   and inputted "all" or "All" or "ALL".
4. The global chanel message and the P2P message won't be mixed together
